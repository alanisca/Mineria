
<?php
class Isla extends Controllers{
	public function __construct(){
		parent::__construct();
	}
	public function isla(){
		// $this->views->getView($this,"isla02",$data);
	}
	public function isla01(){
		$data['page_id'] = "isla01";
		$data['page_tag'] = "Proyecto minero";
		$data['page_title'] = "Islas";
		$data['page_name'] = "Islas Page";
		$data['page_content'] = "Contenido de isla 1";
		$this->views->getView($this,"isla01",$data);
	}
	public function isla02(){
		$data['page_id'] = "isla02";
		$data['page_tag'] = "Proyecto minero";
		$data['page_title'] = "Islas";
		$data['page_name'] = "Islas Page";
		$data['page_content'] = "Contenido de isla 2";
		$this->views->getView($this,"isla02",$data);
	}
}
?>