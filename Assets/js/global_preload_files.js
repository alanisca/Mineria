const loader = new THREE.FileLoader();
THREE.Cache.enabled = true;
var totalVideo = 0;
var totalLoad = 0;
var totalLoaded = 0;
var loading = 0;
var open_scene = true;
anime.timeline({
    easing:"linear",
    begin:function(){
        $(".preloader-content").css("display","block");
    },
    complete:function(){
        preLoad();
    }
}).
add({
    targets:".logo_contain",
    duration:1500,
    opacity: 1,
}).
add({
    targets:".preloader",
    duration:1500,
    background: "#F4E6CD",
});

function loadFiles(file, playOn = 100, video = "false"){
    console.log(video+"Hola maaas ");

    loader.load(
        file,
        function ( data ){
            totalLoaded++;
            if(loading >= playOn || loading === 0) {
                if(video == "false") openScene();
                else openVideoScene();
            }
        },
        function ( xhr ) {
            totalLoad = parseInt((xhr.loaded / xhr.total * 100).toFixed());
            loading = (totalLoad/totalVideo)+((totalLoaded*(100/totalVideo)));
            // console.log((xhr.loaded / xhr.total * 100) + '% loaded' );
            // console.log("Cargando: "+loading);
            $(".loading").css("width", loading+"%");
            if(loading >= playOn) 
                if(video == "false") openScene();
                else openVideoScene();
        },
        function ( err ) {
            console.log("Error: "+err);
        }
    );
}
function changePage(data){
    anime({
        targets:".preloader",
        opacity:1,
        duration:1500,
        begin:function(){
            $(".preloader-content").css("display","block");
            $(".logo_contain").css("display","none");
            $(".preloader").css("background","#ffffff")
        },
        complete:function(){
            location.href=base_url+"isla/"+data;
        }
    });
}
function preLoad(){
    if($(".preload_file").length > 0){
        $(".preload_file").each(function(index){
            loadFiles($(this).attr("src"), $(this).attr("playOn"), $(this).attr("playVideo"));
            totalVideo++;
        });
    }
    else{
        setTimeout(() => {
            $(".preloader-content").animate({opacity:0},2000,function(){
                $(".preloader-content").css("display","none");
            });
        }, 2000);
    }
}
function openScene(){
    if(!open_scene) return;
    open_scene = false;
    console.log("openScene");
    anime.timeline({
        easing:"linear",
        duration:"1000"
    }).add({
        //Se carga la barra al 100%
        targets: ".loading",
        "width":"100%",
        duration:500,
        complete:loaded()
    }).add({
        targets:".preloader",
        "opacity":"0",
        duration:1000,
        complete:function(){
            $(".preloader-content").css("display","none");
        }
    }).add({
        targets: ".top.bar",
        "opacity":"0",
        duration: "1000",
        easing: "linear"
    });
}
function openVideoScene(){
    if(!open_scene) return;
    open_scene = false;
    console.log("openVideoScene");
    anime.timeline({
        easing:"linear",
        duration:"1000"
    }).add({
        targets: ".logo_contain",
        opacity: 0,
        duration:"1000"
    }).add({
        targets:".playbutton",
        opacity:1,
        duration:"1000"
    })
}
$(".playbutton").on(clickHandler,function(){
    loaded();
    anime({
        targets: ".preloader",
        opacity:0,
        duration:2000,
        delay:1000,
        complete: function(){
            $(".preloader-content").css("display","none");
        }
    });
});