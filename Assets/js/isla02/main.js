function loaded(){
    document.querySelector("#videoEsphere01").components.material.material.map.image.play();
}
AFRAME.registerComponent('scenelistener',{
    init:function(){
        this.el.sceneEl.addEventListener('enter-vr',function(){
            document.querySelector("#videoEsphere01").components.material.material.map.image.play();
            document.querySelector("#cursor").setAttribute('visible', true);
        });
        this.el.sceneEl.addEventListener('exit-vr',function(){
            document.querySelector("#cursor").setAttribute('visible', false);
        });
    }
});
AFRAME.registerComponent('imgoptions',{
    init:function(){
        // var myEl = document.querySelector('#image-sound');
        this.el.addEventListener('click', function(){
            console.log("haha",$(this).attr("data-video"));
            
            document.querySelector("#videoEsphere01").components.material.material.map.image.pause();
            document.querySelector('#videoEsphere01').setAttribute('src', $(this).attr("data-video"));
            setTimeout(() => {
                document.querySelector("#videoEsphere01").components.material.material.map.image.play();
            }, 1000);
            // this.a_image.setAttribute("src", );
            // this.a_image.components.material.flushToDOM(true);
        });
    }
});