$( document ).ready(function() {
const loader = new THREE.FileLoader();
var totalVideo = 0;
var totalLoad = 0;
var totalLoaded = 0;
var loading = 0;
var arr = [];
totalVideo = $("video").length;
$("video").each(function(index){
    arr.push(($(this).attr("src")));
});
loadVideo(arr);
function loadVideo(arrSrc){
    loader.load(
        // resource URL

        arrSrc[arrSrc.length-1],
        // onLoad callback
        function ( data ) {
            totalLoaded++;

            if(arrSrc.length > 1){
                arrSrc.pop()
                loadVideo(arrSrc);
            }
            else{
                setTimeout(() => {
                    $(".btn#get-started").fadeIn(2000);
                }, 2000);
                $(".preloader-inner").fadeOut(2000);
            }
        },
        // onProgress callback
        function ( xhr ) {
            console.log("Cargados: "+totalLoaded+" ");
            totalLoad = parseInt((xhr.loaded / xhr.total * 100).toFixed());
            loading = (totalLoad/totalVideo)+((totalLoaded*(100/totalVideo)))
            $(".loader").css(
                "width", loading+"%"
            );
        },
        // onError callback
        function ( err ) {
            $(".load").html( 'An error happened',err );
        }
    );
    
}
AFRAME.registerComponent('play',{
    init:function(){
        // var myEl = document.querySelector('#image-sound');
        this.el.addEventListener('click', function(){
            console.log("hola");
            document.querySelector("#persona").components.material.material.map.image.play();
            document.querySelector('#persona-loop').setAttribute('visible', false);
            document.querySelector('#persona').setAttribute('visible', true);
        });
    }
});
$("#loop04").on('ended', function(){
    console.log("fin");
    document.querySelector('#persona-loop').setAttribute('visible', true);
    document.querySelector('#persona').setAttribute('visible', false);
    // document.getElementById('loop').play();
    // anime({
    //     targets:"#suma",
    //     opacity:0,
    //     duration:"5000"
    // });
    // anime({
    //     targets:"#loop-esfera",
    //     opacity:1,
    //     duration:"5000"
    // });
});
$(".btn#get-started").click(function(){
    $("#preloader-active").fadeOut(2000);
    document.querySelector("#globo").components.material.material.map.image.play();
    document.querySelector("#palma").components.material.material.map.image.play();
    document.querySelector("#persona-loop").components.material.material.map.image.play();
    document.querySelector("#image-sound").components.sound.playSound();
    // document.getElementById('image-sound').play();
});
AFRAME.registerComponent('imgoptions',{
    init:function(){
        // var myEl = document.querySelector('#image-sound');
        this.el.addEventListener('click', function(){
            console.log("adios"+$(this).attr("data-id"));

            document.getElementById('escene02-loop').pause();
            document.getElementById('escene02-loop').currentTime = 0;
            document.getElementById('escene02-sumando2mas2').pause();
            document.getElementById('escene02-sumando2mas2').currentTime = 0;
            document.getElementById('escene02-sumando2mas3').pause();
            document.getElementById('escene02-sumando2mas3').currentTime = 0;
            document.getElementById('escene02-sumando2mas4').pause();
            document.getElementById('escene02-sumando2mas4').currentTime = 0;

            document.getElementById('escene01-audioLoop').pause();
            document.getElementById('escene01-audioLoop').currentTime = 0;
            document.getElementById('escene01-loop01').pause();
            document.getElementById('escene01-loop01').currentTime = 0;
            document.getElementById('escene01-loop02').pause();
            document.getElementById('escene01-loop02').currentTime = 0;
            document.getElementById('escene01-loop03').pause();
            document.getElementById('escene01-loop03').currentTime = 0;
            document.getElementById('escene01-loop04').pause();
            document.getElementById('escene01-loop04').currentTime = 0;

            if($(this).attr("data-id") == "01"){
                document.querySelector('#sky01').setAttribute('visible', true);
                document.querySelector('#sky02').setAttribute('visible', false);
                document.querySelector("#globo").components.material.material.map.image.play();
                document.querySelector("#palma").components.material.material.map.image.play();
                document.querySelector("#persona-loop").components.material.material.map.image.play();
                document.querySelector("#image-sound").components.sound.playSound();
            }
            else{
                document.querySelector('#sky02').setAttribute('visible', true);
                document.querySelector('#sky01').setAttribute('visible', false);
                document.querySelector("#videoesfera").components.material.material.map.image.play();
                document.querySelector("#loop-esfera").components.material.material.map.image.play();
            }
        });
    }
});

});