$( document ).ready(function() {
    AFRAME.registerComponent('change-video', {
        init: function () {
            var el = this.el;
            el.addEventListener('click', function () {
                anime({
                    targets:"#loop-esfera",
                    opacity:0,
                    duration:"5000"
                });
                document.getElementById('escene02-loop').pause();
                document.getElementById('escene02-loop').currentTime = 0;
                document.getElementById('escene02-sumando2mas2').pause();
                document.getElementById('escene02-sumando2mas2').currentTime = 0;
                document.getElementById('escene02-sumando2mas3').pause();
                document.getElementById('escene02-sumando2mas3').currentTime = 0;
                document.getElementById('escene02-sumando2mas4').pause();
                document.getElementById('escene02-sumando2mas4').currentTime = 0;
                var video = $(this).attr("data-video");
                console.log("vide:",video);
                $("#suma").attr("src","#"+video);
                document.getElementById(video).play();
                anime({
                    targets:"#suma",
                    opacity:1,
                    duration:"5000"
                });
                $("#"+video).on('ended', function(){
                    document.getElementById('escene02-loop').play();
                    anime({
                        targets:"#suma",
                        opacity:0,
                        duration:"5000"
                    });
                    anime({
                        targets:"#loop-esfera",
                        opacity:1,
                        duration:"5000"
                    });
                });
            })
        }
    });
});
