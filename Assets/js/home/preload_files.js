const loader = new THREE.FileLoader();
THREE.Cache.enabled = true;
var totalVideo = 0;
var totalLoad = 0;
var totalLoaded = 0;
var loading = 0;
var arrPreload = [];
if($(".preload_file").length > 0){
    $(".preload_file").each(function(index){
        loadFiles($(this).attr("src"));
        totalVideo++;
    });
}
else{
    setTimeout(() => {
        $(".preloader-content").animate({opacity:0},2000,function(){
            $(".preloader-content").css("display","none");
        });
    }, 2000);
}
function loadFiles(file){
    loader.load(
        file,
        function ( data ){
            console.log("Cargado completamente"+loading);
            totalLoaded++;
            if(loading >= 100) {
                var vScale = 1;
                if($(window).width() <= 768){
                    vScale = .7;
                    $("#camera").attr("rotation","-40 10 0");
                } 
                if($(window).width() <= 540) {
                    $("#camera").attr("rotation","-50 10 0");
                    vScale = .3;
                }
                $("#islas").attr("scale",`${1*vScale} ${1*vScale} ${1*vScale}`);

                // $(".preloader-content").animate({opacity:0},2000,function(){
                    // $(".preloader-content").css("display","none");
                    anime({
                        targets: ".top.bar",
                        "height":"0%",
                        duration: "1000",
                        easing: "linear",
                        complete: function(){
                            $(".change_scene").css("display","none");
                            
                        }
                    });
                    
                // });
            }
        },
        function ( xhr ) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded' );
            totalLoad = parseInt((xhr.loaded / xhr.total * 100).toFixed());
            loading = (totalLoad/totalVideo)+((totalLoaded*(100/totalVideo)));
            console.log("Cargando: "+loading);
            $(".loading").css(
                "width", loading+"%"
            );
        },
        function ( err ) {
            console.log("Error: "+err);
        }
    );
}