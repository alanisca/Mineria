AFRAME.registerComponent('marcador_isla', {
    init: function () {
        this.el.addEventListener('click', function(){
            var stringToLog = $(this).attr("marcador_isla");
            changePage(stringToLog);
        });
    }
});

function loaded(){
    anime({
        targets: "#isla1",
        visible:true,
        scale: function(){
            return `.0007 .0007 .0007`;
        },
        easing: 'easeOutExpo',
        duration: 5000
    });
    anime({
        targets: ".top_bar",
        top:"0",
        duration: 2000,
        easing: 'linear'
    });
    anime({
        targets:"#isla1 .marcador",
        position: function(el, i, l){
            var p = anime.get($('.marcador')[i],"position");
            return [p.x+" "+p.y+" "+p.z, p.x+" "+(870)+" "+p.z];
        },
        duration:"1000",
        easing: 'linear',
        delay:"1000"
    });
}