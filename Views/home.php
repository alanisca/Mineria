<!doctype html>
<html lang="es">
    <head>
        <?= headerAdmin($data); ?>
        <script src="https://unpkg.com/aframe-orbit-controls@1.2.0/dist/aframe-orbit-controls.min.js"></script> 
        <script src="<?= media(); ?>plugins/aFrame/aframe-event-set-component.min.js"></script>
    </head>
    <body>
    <!-- ? Preloader Start -->
    <?= preloadAdmin(); ?>
    <!-- ? Preloader End -->
    
    <a-scene cursor="rayOrigin: mouse" vr-mode-ui="enabled: false">
        <div class="top_bar"></div>
        <a-assets>
            <a-asset-item autoload class="preload_file" id="island-model1" src="<?= media(); ?>content/01_intro/prueba1/scene.gltf"></a-asset-item>
            <!-- <a-video class="preload_file" id="video02_01" autoplay="false" src="<?= media(); ?>content/isla_02/video/background01.mp4"></a-video> -->
        </a-assets>
        <a-entity id="islas">
            <a-entity id="isla1" gltf-model="#island-model1" position="0 0 0" scale="0 0 0" visible="false" rotation="0 0 0" visible="false" drag-rotate-component>
                <a-entity class="marcador" scale="100 100 100" position="-2012.95078 375 -884.87216" marcador_isla="isla02">
                    <a-cylinder material="" geometry="height: 3.99; radius: 0.4"></a-cylinder>
                    <a-sphere material="color: #ff0000" geometry="" position="0 1.7963 0"></a-sphere>
                </a-entity>
                <a-entity class="marcador" scale="100 100 100" position="408.32078 375 1146.81591" marcador_isla="isla02">
                    <a-cylinder material="" geometry="height: 3.99; radius: 0.4"></a-cylinder>
                    <a-sphere material="color: #ff0000" geometry="" position="0 1.7963 0"></a-sphere>
                </a-entity>
                <a-entity class="marcador" scale="100 100 100" position="-283.81426 375 -1538.28385" marcador_isla="isla02">
                    <a-cylinder material="" geometry="height: 3.99; radius: 0.4"></a-cylinder>
                    <a-sphere material="color: #ff0000" geometry="" position="0 1.7963 0"></a-sphere>
                </a-entity>
            </a-entity>
        </a-entity>        
        <a-entity   id="camera" 
                    camera="" 
                    look-controls="false" 
                    rotation="0 0 0"
                    orbit-controls="target: 0 0 0; minDistance: 0.5; maxDistance: 180; initialPosition: 0 3 2; zoomSpeed:.1;maxDistance:3.5;minDistance:2.5;maxPolarAngle:50;minPolarAngle:50;" 
                    data-aframe-inspector-original-camera="">
                    <!-- orbit-controls="target: 0 0 0; minDistance: 0.5; maxDistance: 180; initialPosition: 0 4 3.5; zoomSpeed:.1;maxPolarAngle:50;minPolarAngle:50;" -->
        </a-entity>
    </a-scene>

    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>js/global_preload_files.js" ></script>
    <!-- <script src="<?= media(); ?>plugins/aFrame/aframe-inspector.min.js"></script> -->
    </body>
</html>


