<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <meta name="apple-mobile-web-app-capable" content="yes">
</head>
<body>
    <!-- ? Preloader Start -->
    <?= preloadAdmin(); ?>
    <!-- ? Preloader End -->
    <a-scene cursor="rayOrigin: mouse" vr-mode-ui="enabled: true" scenelistener>
        <a-assets>
            <video class="preload_file" id="video_01" crossorigin="anonymous" playOn="10" playVideo="true" webkit-playsinline playsinline autoload src="<?= media(); ?>content/isla_02/video/background01.mp4"></video>
            <video class="preload_file" id="video_02" crossorigin="anonymous" loop playVideo="true" webkit-playsinline playsinline autoload src="<?= media(); ?>content/isla_02/video/background02.mp4"></video>
            <img class="" id="video_01-TH"  src="<?= media(); ?>content/isla_02/img/grupo01.jpg">
            <img class="" id="video_02-TH"  src="<?= media(); ?>content/isla_02/img/grupo02.jpg">
        </a-assets>
        <a-sky id="videoEsphere01" src="#video_01" rotation="0 -90 0" playsinline>
        <a-image    src="#video_01-TH" 
                    data-video="#video_01"
                    width="1"
                    height="1"
                    rotation="0 0 0"
                    position="2 0 3" 
                    event-set__mouseenter="scale: 1.2 1.2 1; dur: 500" 
                    event-set__mouseleave="scale: 1 1 1; dur: 500" 
                    data-id="02" imgoptions></a-image>
        <a-image    src="#video_02-TH" 
                    data-video="#video_02"
                    width="1"
                    height="1"
                    rotation="0 0 0"
                    position="0 0 3" 
                    event-set__mouseenter="scale: 1.2 1.2 1; dur: 500" 
                    event-set__mouseleave="scale: 1 1 1; dur: 500" 
                    data-id="02" imgoptions></a-image>
        </a-sky>
        <a-entity camera look-controls>
            <a-cursor
              id="cursor"
              material="color: #D0A153; shader: flat"
              animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 500"
              animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500" visible="false"></a-cursor>
          </a-entity>
    </a-scene>

    <?= footerAdmin($data); ?>
    <script type="text/javascript" src="<?= media(); ?>js/global_preload_files.js"></script>
</body>
</html>