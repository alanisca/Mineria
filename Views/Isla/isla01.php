<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <link href="<?= media(); ?>css/loading.css" rel="stylesheet">
    <link href="<?= media(); ?>css/main.css" rel="stylesheet">
</head>
<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active" >
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="<?= media(); ?>images/logo/logotext@2x.png" alt="">
                </div>
            </div>
            <a href="#" id="get-started" class="btn  mt-30">Iniciar videos</a>
        </div>
        <div class="loader"></div>
    </div>
    <!-- Preloader Start -->
    <a-scene>
        <a-assets>
            <img id="option01" crossorigin="anonymous" src="<?= media(); ?>content/isla_01/img/grupo01.jpg">
            <img id="option02" crossorigin="anonymous" src="<?= media(); ?>content/isla_01/img/grupo02.jpg">
            <img id="option03" crossorigin="anonymous" src="<?= media(); ?>content/isla_01/img/grupo03.jpg">
            <img id="logo" src="<?= media(); ?>images/logo/logo@2x.png">

            
            <img id="escene01-background" crossorigin="anonymous" src="<?= media(); ?>content/isla_01/img/scene01/background.jpg"></img>
            <audio id="escene01-audioLoop" crossorigin="anonymous" src="<?= media(); ?>content/isla_01/audio/loop.mp3" preload="auto" loop></audio>
            <video class="loop01" crossorigin="anonymous"  id="escene01-loop01" autoload loop src="<?= media(); ?>content/isla_01/video/scene01/loop01.mp4"></video>
            <video class="loop02" crossorigin="anonymous"  id="escene01-loop02" autoload loop src="<?= media(); ?>content/isla_01/video/scene01/loop02.mp4"></video>
            <video class="loop03" crossorigin="anonymous"  id="escene01-loop03" autoload loop src="<?= media(); ?>content/isla_01/video/scene01/loop03.mp4"></video>
            <video class="loop04" crossorigin="anonymous"  id="escene01-loop04" autoload      src="<?= media(); ?>content/isla_01/video/scene01/loop04.mp4"></video>


            <img id="escene02-background" src="<?= media(); ?>content/isla_01/img/scene02/background.jpg"></img>
            <video class="360video" id="escene02-loop" autoload loop src="<?= media(); ?>content/isla_01/video/scene02/loop.mp4"></video>
            <video class="360video" id="escene02-ventilador" autoload loop src="<?= media(); ?>content/isla_01/video/scene02/Nested.mp4"></video>
            <video class="360video" id="escene02-sumando2mas2"  autoload src="<?= media(); ?>content/isla_01/video/scene02/2mas2.mp4"></video>
            <video class="360video" id="escene02-sumando2mas3"  autoload src="<?= media(); ?>content/isla_01/video/scene02/2mas3.mp4"></video>
            <video class="360video" id="escene02-sumando2mas4"  autoload src="<?= media(); ?>content/isla_01/video/scene02/2mas4.mp4"></video>

        </a-assets>

        <a-sky id="sky01" src="#escene01-background" rotation="0 0 0">
            <a-videosphere id="globo" src="#escene01-loop01" geometry="radius: 490; phiLength: 35.4; phiStart: 43.5; thetaLength: 17.65; thetaStart: 94" sound="src: #audioLoop;" material="" visible="true"></a-videosphere>
            <a-videosphere id="palma" src="#escene01-loop02" geometry="radius: 490; phiLength: 54.62; phiStart: 332.69; thetaLength: 42.38; thetaStart: 39.38" material="" visible="true"></a-videosphere>
            <a-videosphere id="persona-loop" src="#escene01-loop03" geometry="radius: 490; segmentsHeight: 21; phiLength: 35.04; phiStart: 345; thetaLength: 35; thetaStart: 79.48" material="" visible=""></a-videosphere>
            <a-videosphere id="persona" src="#escene01-loop04" geometry="radius: 490; segmentsHeight: 21; phiLength: 35.04; phiStart: 346.88; thetaLength: 35; thetaStart: 79.48" material="" visible="false"></a-videosphere>
            <a-image id="image-sound" 
                    sound="src: <?= media(); ?>content/isla_01/audio/loop.mp3; loop: true" 
                    src="#logo" 
                    width="1" 
                    height="1" 
                    position="4 0 0" 
                    material="" 
                    geometry="width: 0.5; height: 0.5" 
                    event-set__mouseenter="scale: 1.2 1.2 1; dur: 500"
                    event-set__mouseleave="scale: 1 1 1; dur: 500"
                    rotation="0 90 0" play>
                </a-image>
        </a-sky>

        <a-sky id="sky02" src="#escene02-background" rotation="0 -90 0" visible="false">
            <a-videosphere id="videoesfera" src="#escene02-ventilador" material="" geometry="radius: 490; phiLength: 35; phiStart: 27.25; thetaLength: 35.28; thetaStart: 101.11"></a-videosphere>
            <a-videosphere id="loop-esfera" src="#escene02-loop" geometry="radius: 490; phiLength: 56; phiStart: 330.89; thetaLength: 84.66; thetaStart: 67.63" material="" opacity="1"></a-videosphere>
            <a-videosphere id="suma" src="" geometry="radius: 480; phiLength: 56; phiStart: 330.54; thetaLength: 84.62; thetaStart: 67.63" material="" opacity="0"></a-videosphere>
            <a-text id="2mas2" color="white" value="2 + 2" data-video="escene02-sumando2mas2" geometry="primitive: plane; width: 1; height:.5;" material="color: blue" text="align: center;" position="-2 0 -6" change-video></a-text>
            <a-text id="2mas3" color="white" value="2 + 3" data-video="escene02-sumando2mas3" geometry="primitive: plane; width: 1; height:.5;" material="color: blue" text="align: center;" position="0 0 -6" change-video></a-text>
            <a-text id="2mas4" color="white" value="2 + 4" data-video="escene02-sumando2mas4" geometry="primitive: plane; width: 1; height:.5;" material="color: blue" text="align: center;" position="2 0 -6" change-video></a-text>
        </a-sky>
        <a-image  src="#option01" width="1" height="1" rotation="0 180 0" position="2 0 3" event-set__mouseenter="scale: 1.2 1.2 1; dur: 500" event-set__mouseleave="scale: 1 1 1; dur: 500" data-id="02" imgoptions></a-image>
        <a-image src="#option02" width="1" height="1" rotation="0 180 0" position="0 0 3" event-set__mouseenter="scale: 1.2 1.2 1; dur: 500" event-set__mouseleave="scale: 1 1 1; dur: 500" data-id="01" imgoptions></a-image>
        <a-entity camera look-controls>
            <a-cursor
                id="cursor"
                material="color: #D0A153; shader: flat"
                animation__click="property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1 1 1; dur: 500"
                animation__fusing="property: fusing; startEvents: fusing; from: 1 1 1; to: 0.1 0.1 0.1; dur: 1500">
            </a-cursor>
        </a-entity>
    </a-scene>
    <?= footerAdmin(); ?>
    <script src="<?= media(); ?>js/global_preload_files.js" ></script>
    <script src="<?= media(); ?>js/isla01/main.js" ></script>
    <script src="<?= media(); ?>js/isla01/main02.js" ></script>
</body>
</html>