<script>
    var clickHandlerDown = ('ontouchstart' in document.documentElement ? "touchstart" : "mousedown");
    var clickHandlerUp = ('ontouchend' in document.documentElement ? "touchend" : "mouseup");
    var clickHandler = ('ontouchstart' in document.documentElement ? "click" : "click");
</script>
<script src="<?= media(); ?>plugins/jQuery/jquery-3.6.0.min.js"></script>
<script src="<?= media(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= media(); ?>js/<?= $data["page_id"]; ?>/main.js"></script>

<script>
    const base_url = "<?= base_url();?>";
</script>