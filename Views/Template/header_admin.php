<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="mage/x-icon" href="<?= media(); ?>images/logo/favicon.ico">
<title><?= $data["page_tag"] ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?= media(); ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="<?= media(); ?>css/loading.css" rel="stylesheet">
<link href="<?= media(); ?>css/<?= $data["page_id"]; ?>/main.css" rel="stylesheet">
<script src="<?= media(); ?>plugins/aFrame/aframe.min.js"></script>
<script src="<?= media(); ?>plugins/aFrame/anime.min.js"></script>