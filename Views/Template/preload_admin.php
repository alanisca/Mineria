<div class="preloader-content">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="position-relative logo_contain">
            <div class="preload-bar">
                <div class="load">
                    <div class="loading"></div>
                </div>
            </div>
            <div class="preload-img">
                <img src="<?= media(); ?>images/logo/logo-texto@2x.png" class="d-none d-sm-block d-md-block" alt="" srcset="">
                <img src="<?= media(); ?>images/logo/logo@2x.png" class="d-block d-sm-none d-md-none" alt="" srcset="">
            </div>
        </div>
        <button type="button" class="btn btn-primary playbutton">Play</button>
    </div>
</div>